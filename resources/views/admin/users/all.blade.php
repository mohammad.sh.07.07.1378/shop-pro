@component('admin.layout.content',['title'=>'مدیریت کاربران'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل مدیریت</a></li>
        <li class="breadcrumb-item active">کاربران</li>
    @endslot
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"> کاربران</h3>

                    <div class="card-tools d-flex float-left">
                        <div class="input-group input-group-sm d-flex" style="width: 200px;">
                            <form action="" class="d-flex">
                                <input type="text" name="search" class="form-control float-right"
                                       placeholder="جستجو">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>

                        <div class="float-left  mr-2">
                            @can('create-user')
                                <a class="btn btn-sm btn-info" href="{{route('admin.users.create')}}">ایجاد کاربر</a>
                            @endcan
                            @can('show-staff-user')
                                <a class="btn btn-sm btn-warning"
                                   href="{{ request()->fullUrlWithQuery(['admin'=>1]) }}">
                                    کاربران مدیر
                                </a>
                        </div>
                        @endcan
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>آیدی</th>
                            <th>نام</th>
                            <th>ایمیل</th>
                            <th>وضعیت ایمیل</th>
                            <th>عملیات</th>
                        </tr>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @if($user->email_verified_at)
                                        <span class="badge badge-success">تایید شده</span>
                                    @else
                                        <span class="badge badge-danger">تایید نشده</span>
                                    @endif
                                </td>
                                <td class="d-flex btn-sm">
                                    @can('delete-user')
                                        <form action="{{route('admin.users.destroy',['user'=>$user->id])}}"
                                              method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger ml-2">حذف</button>
                                        </form>
                                    @endcan
                                    @can('edit-user')
                                        <a href="{{ route('admin.users.edit',['user'=>$user->id]) }}"
                                           class="btn btn-sm btn-primary">ویرایش</a>
                                    @endcan
                                    @if($user->is_staff)
                                        @can('staff-user-permission')
                                            <a href="{{ route('admin.user.permission',$user->id) }}"
                                               class="btn btn-sm btn-light mr-2">
                                                دسترسی ها
                                            </a>
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{$users->render()}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>


@endcomponent
