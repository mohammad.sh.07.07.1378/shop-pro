@component('admin.layout.content',['title'=>'مدیریت نظرات'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل نظرات</a></li>
        <li class="breadcrumb-item active">نظرات</li>
    @endslot
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"> نظرات</h3>

                    <div class="card-tools d-flex float-left">
                        <div class="input-group input-group-sm d-flex" style="width: 200px;">
                            <form action="" class="d-flex">
                                <input type="text" name="search" class="form-control float-right"
                                       placeholder="جستجو">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>آیدی</th>
                            <th>نام کاربر</th>
                            <th>ایمیل</th>
                            <th>متن نظر</th>
                            <th>عملیات</th>
                        </tr>
                        @foreach($comments as $comment)
                            <tr>
                                <td>{{$comment->id}}</td>
                                <td>{{$comment->user->name}}</td>
                                <td>{{$comment->user->email}}</td>
                                <td>{{$comment->comment}}</td>
                                <td class="d-flex btn-sm">
                                    @can('delete-comment')
                                        <form action="{{route('admin.comments.destroy',['comment'=>$comment->id])}}"
                                              method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger ml-2">حذف</button>
                                        </form>
                                    @endcan
                                    @can('unapproved-comment')
                                        <form action="{{ route('admin.comments.update',['comment'=>$comment->id]) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary">تایید کردن
                                            </button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{$comments->render()}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>


@endcomponent
