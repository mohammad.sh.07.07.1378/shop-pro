@component('admin.layout.content',['title'=>'مقام ها'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل مقام ها</a></li>
        <li class="breadcrumb-item active">لیست مقام ها</li>
    @endslot
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">مقام ها</h3>

                    <div class="card-tools d-flex float-left">
                        <div class="input-group input-group-sm d-flex" style="width: 200px;">
                            <form action="" class="d-flex">
                                <input type="text" name="search" class="form-control float-right"
                                       placeholder="جستجو">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>

                        <div class="float-left  mr-2">
                            @can('create-role')
                                <a class="btn btn-sm btn-info" href="{{route('admin.role.create')}}">ایجاد مقام</a>
                            @endcan
                        </div>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>نام</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->name}}</td>
                                <td>{{$role->description}}</td>
                                <td class="d-flex btn-sm">
                                    @can('delete-role')
                                        <form action="{{route('admin.role.destroy',$role->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger ml-2">حذف</button>
                                        </form>
                                    @endcan
                                    @can('edit-role')
                                        <a href="{{ route('admin.role.edit',$role->id) }}"
                                           class="btn btn-sm btn-primary">ویرایش</a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{$roles->render()}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>


@endcomponent
