@component('admin.layout.content',['title'=>'ایجاد دسترسی'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل دسترسی ها</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.permission.index') }}">دسترسی ها</a></li>
        <li class="breadcrumb-item active">ویرایش دسترسی</li>
    @endslot
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ویرایش دسترسی</div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>
                                        {{ $err }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('admin.permission.update',$permission->id)}}" class="p-3">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">نام دسترسی :</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       required autocomplete="name" autofocus value="{{$permission->name}}">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description"
                                   class="col-md-4 col-form-label text-md-right">توضیحات دسترسی : </label>

                            <div class="col-md-6">
                                <input id="description" type="text"
                                       class="form-control @error('email') is-invalid @enderror" name="description"
                                       value="{{ $permission->description }}" required autocomplete="description">

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    ویرایش دسترسی
                                </button>
                                <a href="{{ route('admin.permission.index') }}" class="btn btn-default mr-3">
                                    انصراف
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endcomponent
