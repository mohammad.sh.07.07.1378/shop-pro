@extends('profile.layout')

@section('main')

    <h4>
        Verify Phone Number
    </h4>

    {{--    @if($errors->any())--}}
    {{--        <div class="alert alert-danger">--}}
    {{--            <ul>--}}
    {{--                @foreach($errors->all() as $err)--}}
    {{--                    <li>--}}
    {{--                        {{ $err }}--}}
    {{--                    </li>--}}
    {{--                @endforeach--}}
    {{--            </ul>--}}
    {{--        </div>--}}
    {{--    @endif--}}
    <hr>
    <form action="{{ route('verifyPhone') }}" method="POST">
        @csrf
        <div class="form-group">
            <labale for="" class="form-group d-block mb-1">Write Token</labale>
            <input class="form-control @error('token') is-invalid @enderror" name="token" placeholder="Write Token">
            @error('token')
            <span class="invalid-feedback">
                <strong>
                    {{$message}}
                </strong>
            </span>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">
            submit
        </button>
    </form>

@endsection
