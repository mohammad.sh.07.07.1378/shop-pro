@extends('profile.layout')

@section('main')
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ای دی محصول</th>
                <th>نام</th>
                <th>تعداد سفارش</th>
                <th>قیمت تکی</th>
                <th>قیمت کل</th>
            </tr>
            @foreach($detailOrder as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->title}}</td>
                    <td>{{$product->pivot->quantity}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->price * $product->pivot->quantity }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{ route('profile.order.list') }}"
           class="btn btn-sm btn-primary">بازگشت
        </a>
    </div>
@endsection
