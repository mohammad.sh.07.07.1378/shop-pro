@extends('profile.layout')

@section('main')

    <h4>
        Two Factor Auth
    </h4>

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $err)
                    <li>
                        {{ $err }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <hr>
    <form action="#" method="POST">
        @csrf
        <div class="form-group">
            <labale for="" class="form-group d-block mb-1">Type</labale>
            <select name="type" class="form-control">
                @foreach(config('twofactor') as $key=>$val)
                <option value="{{$key}}" class="form-text" {{ old('type') == $key ||auth()->user()->two_factor_auth == $key ?'selected' : ''}}>
                    {{$val}}
                </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <labale for="" class="form-group d-block mb-1">Phone Number</labale>
            <input class="form-control" name="phone" placeholder="Add phone number"
                   value="{{old('phone') ?? auth()->user()->phone}}">
        </div>

        <button type="submit" class="btn btn-primary">
            update
        </button>
    </form>

@endsection
