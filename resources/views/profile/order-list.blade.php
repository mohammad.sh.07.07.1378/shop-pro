@extends('profile.layout')

@section('main')
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>شماره سفارش</th>
                <th>تاریخ</th>
                <th>وضعیت پرداخت</th>
                <th>کد رهگیری</th>
                <th>عملیات</th>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{jdate($order->create_at)->format('%d %B %Y')}}</td>
                    <td>{{$order->status}}</td>
                    <td>{{$order->tracing_seial ?? 'نیست'}}</td>
                    <td class="d-flex btn-sm">

                        <a href="{{ route('profile.order.detail',$order->id) }}"
                           class="btn btn-sm btn-primary">جزئیات محصول</a>
                        @if($order->status=='unpaid')
                            <a  href="{{ route('profile.order.payment',$order->id) }}"
                               class="btn btn-sm btn-warning mr-2">پرداخت سفارش</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
