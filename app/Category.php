<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Discount\Entities\Discount;

class Category extends Model
{
    protected $fillable = ['name', 'parent'];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function child()
    {
        return $this->hasMany(Category::class, 'parent', 'id');
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }
}
