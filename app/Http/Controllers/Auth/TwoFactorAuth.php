<?php

namespace App\Http\Controllers\Auth;

use App\ActiveCode;
use App\Notifications\SendEmailNotification;
use App\Notifications\SendSMSnotifcation;
use Illuminate\Http\Request;


trait TwoFactorAuth
{
    public function loginTwoFactorAuth(Request $request, $user)
    {

        if ($user->hasTwoFactorAuthOFF()) {
            return $this->logoutAndEnterYourToken($request, $user);
        }

        $request->user()->notify(new SendEmailNotification);

        return false;
    }

    public function logoutAndEnterYourToken(Request $request, $user)
    {
        auth()->logout();
        $request->session()->flash('auth', [
            'user_id' => $user->id,
            'using_sms' => false,
            'remember' => $request->has('remember'),
        ]);
        if ($user->hasTwoFactorAuthSMS()) {

            $code = ActiveCode::generateCode($user);

            $user->notify(new SendSMSnotifcation($code, $user->phone));

            $request->session()->push('auth.using_sms', true);

            return redirect(route('auth.token'));
        }
    }
}
