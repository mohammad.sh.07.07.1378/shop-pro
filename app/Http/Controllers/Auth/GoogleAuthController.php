<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use PHPUnit\Exception;
use Illuminate\Support\Str;

class GoogleAuthController extends Controller
{
    use TwoFactorAuth;

    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback(Request $request)
    {

        try {
            $googleUser = Socialite::driver('google')->user();
            $user = User::where('email', $googleUser->email)->first();

            if (!$user) {
                $user = User::create([
                    'name' => $googleUser->name,
                    'email' => $googleUser->email,
                    'password' => bcrypt(Str::random(16)),
                ]);
            }
            if (!$user->hasVerifiedEmail()) {
                $user->markEmailAsVerified();
            }

            Auth::loginUsingId($user->id);

            alert()->success('Message', 'Welcome')->persistent('Close');

            return $this->loginTwoFactorAuth($request, $user) ?: redirect(route('home'));

        } catch (Exception $e) {
            alert()->error('Error Message', 'Login failed')->persistent('Close');
            return redirect('/login');
        }
    }
}
