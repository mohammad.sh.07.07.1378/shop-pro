<?php

namespace App\Http\Controllers\admin;

use App\Attribute;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::query();

        if ($keyboard = \request('search')) {
            $products->where('name', 'LIKE', "%{$keyboard}%")
                ->orWhere('description', 'LIKE', "%%{$keyboard}%");
        }

        $products = $products->latest()->paginate(20);

        return view('admin.product.all', compact('products'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([
            'title' => ['required', 'max:255'],
            'price' => ['required', 'numeric'],
            'inventory' => ['required', 'numeric'],
            'image' => ['required'],
            'description' => ['required'],
            'categories' => ['required'],
            'attributes' => ['array']
        ]);

//this code work at not file manger
//        $file = $request->file('image');
//        $directory = '/images/' . now()->year . '/' . now()->month . '/' . now()->day . '/';
//        $file->move(public_path($directory), $file->getClientOriginalName());
//
//        $data['image'] = $directory . $file->getClientOriginalName();

        $product = auth()->user()->products()->create($data);
        $product->categories()->sync($data['categories']);
        if (isset($data['attributes'])) {
            $this->attachAttributesToProduct($product, $data);
        }


        alert()->success('ثبت شد');

        return redirect(route('admin.product.index'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $data = $request->validate([
            'title' => ['required', 'max:255', Rule::unique('products')->ignore($product->id), 'string'],
            'price' => ['required', 'numeric'],
            'inventory' => ['required', 'numeric'],
            'image' => ['required'],
            'description' => ['required'],
            'categories' => ['required'],
            'attributes' => ['array']
        ]);

        //this code work at not file manger
//        if ($request->file('image')) {
//            $request->validate([
//                'image' => ['required', 'mimes:jpg,bmp,png']
//            ]);
//
//
//            if (File::exists(public_path($product->image))) {
//                File::delete(public_path($product->image));
//            }
//            $file = $request->file('image');
//            $directory = '/images/' . now()->year . '/' . now()->month . '/' . now()->day . '/';
//            $file->move(public_path($directory), $file->getClientOriginalName());
//
//            $data['image'] = $directory . $file->getClientOriginalName();
//        }


        $product->update($data);

        $product->categories()->sync($data['categories']);

        $product->attributes()->detach();

        if (isset($data['attributes'])) {
            $this->attachAttributesToProduct($product, $data);
        }

        alert()->success('ویرایش شد');

        return redirect(route('admin.product.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        alert()->success('حذف شد');
        return redirect(route('admin.product.index'));
    }

    /**
     * @param $attributes
     * @param $product
     */
    public function attachAttributesToProduct(Product $product, array $data): void
    {
        $attribute = collect($data['attributes']);
        $attribute->each(function ($item) use ($product) {
            if (is_null($item['name']) || is_null($item['value'])) return;

            $attr = Attribute::firstOrCreate([
                'name' => $item['name']
            ]);

            $attrVal = $attr->values()->firstOrCreate([
                'value' => $item['value']
            ]);

            $product->attributes()->attach($attr->id, ['value_id' => $attrVal->id]);

        });
    }
}
