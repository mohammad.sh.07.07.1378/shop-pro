<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function create(Request $request, User $user)
    {

        return view('admin.users.permission', compact('user'));
    }

    public function store(Request $request, User $user)
    {


        $user->permissions()->sync($request->permission);
        $user->roles()->sync($request->role);

        alert()->success('با موفقیت انجام شد');

        return redirect(route('admin.users.index'));

    }
}
