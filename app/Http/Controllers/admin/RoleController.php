<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:show-roles')->only(['index']);
        $this->middleware('can:create-role')->only(['create', 'store']);
        $this->middleware('can:delete-role')->only(['destroy']);
        $this->middleware('can:edit-role')->only(['update', 'edit']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::query();

        if ($keyboard = \request('search')) {
            $roles->where('name', 'LIKE', "%{$keyboard}%")
                ->orWhere('name', 'LIKE', "%{$keyboard}%");
        }

        $roles = $roles->latest()->paginate(20);

        return view('admin.role.all', compact('roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:roles'],
            'description' => ['required', 'max:255', 'string'],
            'permission' => ['required', 'array']
        ]);

        $role = Role::create($data);
        $role->permissions()->sync($data['permission']);

        return redirect(route('admin.role.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $data = $request->validate([
            'name' => ['required', 'max:255', 'string', Rule::unique('roles')->ignore($role->id)],
            'description' => ['required', 'max:255', 'string'],
            'permission' => ['required', 'array']
        ]);
        $role->update($data);
        $role->permissions()->sync($data['permission']);

        alert()->success('ویرایش شد');
        return redirect(route('admin.role.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        alert()->success('حذف شد');
        return redirect(route('admin.role.index'));
    }
}
