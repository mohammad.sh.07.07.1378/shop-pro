<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function comment(Request $request)
    {

//        if (!$request->ajax()) {
//            return response()->json([
//                'status' => 'error not request json'
//            ]);
//        }

        $data = $request->validate([
            'comment' => 'required',
            'commentable_id' => 'required',
            'commentable_type' => 'required',
            'parent_id' => 'required',
        ]);

        auth()->user()->comments()->create($data);

        alert()->success('ثبت شد');

        return back();

//
//        return response()->json([
//            'status' => 'success'
//        ]);

    }
}
