<?php

namespace App\Http\Controllers;

use App\Helper\Cart\Cart;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Payment\Facade\Payment as shetabitPayment;
use Shetabit\Multipay\Invoice;

class PaymentController extends Controller
{
    public function payment()
    {

        $cart = Cart::all();

        if ($cart->count()) {

            $price = $cart->sum(function ($item) {
                return $item['discount_percent'] == 0
                    ? $item['product']->price * $item['quantity']
                    : ($item['product']->price - $item['product']->price * $item['discount_percent']) * ($item['quantity']);

            });

            $product = $cart->mapWithKeys(function ($item) {
                return [$item['product']->id => ['quantity' => $item['quantity']]];
            });


            $order = auth()->user()->orders()->create([
                'price' => $price,
                'status' => 'unpaid'
            ]);

            $order->products()->attach($product);


            $invoice = (new Invoice)->amount(1000);
            return shetabitPayment::callbackUrl(route('payment.callback'))->purchase($invoice, function ($driver, $transactionId) use ($order, $cart) {
                $order->paments()->update([
                    'resnumber' => $transactionId,

                ]);

                $cart->flush();

            })->pay()->render();

        }
        return false;
    }

    public function callback(Request $request)
    {

        try {
            $pay = Payment::where('resnumber', $request->clientrefid)->firstOrFail();
            $receipt = shetabitPayment::amount(1000)->transactionId($request->clientrefid)->verify();
            $pay->orders->update([
                'status' => 'paid'
            ]);
            $pay->update([
                'status' => 1
            ]);

            alert()->success('موفق آمیز بود');

            return redirect('/product');

        } catch (InvalidPaymentException $exception) {

            alert()->error($exception->getMessage());

            return redirect('/product');
        }

    }
}
