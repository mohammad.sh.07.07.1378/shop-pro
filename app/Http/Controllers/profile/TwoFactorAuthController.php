<?php

namespace App\Http\Controllers\profile;

use App\ActiveCode;
use App\Http\Controllers\Controller;
use App\Notifications\SendSMSnotifcation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TwoFactorAuthController extends Controller
{

    public function twofactorauth()
    {
        return view('profile.twofactorauth');
    }

    public function manegeTwoFactorAuth(Request $request)
    {
        $data = $this->validateTwoFactor($request);

        if ($this->typeTwoFactorIsSms($data)) {

            if ($request->user()->phone !== $data['phone']) {

                $this->generateANDsendCode($request, $data['phone']);


            } else {
                $request->user()->update([
                    'two_factor_auth' => 'sms'
                ]);
            }
        }
        if ($this->typeTwoFactorIsOff($data)) {
            $request->user()->update([
                'two_factor_auth' => 'off'
            ]);
        }
        return back();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function validateTwoFactor(Request $request)
    {
        $data = $request->validate([
            'type' => 'required|in:off,sms',
            'phone' => ['required_unless:type,off', Rule::unique('users', 'phone')->ignore($request->user()->id)]
        ]);
        return $data;
    }

    /**
     * @param $type
     * @return bool
     */
    public function typeTwoFactorIsSms($type): bool
    {
        return $type['type'] === 'sms';
    }

    /**
     * @param Request $request
     * @param $phone
     */
    public function generateANDsendCode(Request $request, $phone)
    {
        $code = ActiveCode::GenerateCode($request->user());

        $request->session()->flash('phone', $phone);

        $request->user()->notify(new SendSMSnotifcation($code, $phone));

        return redirect(route('verifyPhone'));
    }

    /**
     * @param $type
     * @return bool
     */
    public function typeTwoFactorIsOff($type): bool
    {
        return $type['type'] === 'off';
    }

}
