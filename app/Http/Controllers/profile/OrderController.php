<?php

namespace App\Http\Controllers\profile;

use App\Helper\Cart\Cart;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment as shetabitPayment;

class OrderController extends Controller
{
    public function index()
    {
        $orders = auth()->user()->orders;
        return view('profile.order-list', compact('orders'));
    }

    public function showDetail(Order $order)
    {
        $this->authorize('view', $order);
        $detailOrder = $order->products()->latest()->paginate(10);
        return view('profile.order-detail', compact('detailOrder'));
    }

    public function payment(Order $order)
    {
        $this->authorize('view', $order);
        $invoice = (new Invoice)->amount(1000);
        return shetabitPayment::callbackUrl(route('payment.callback'))->purchase($invoice, function ($driver, $transactionId) use ($order) {
            $order->paments()->update([
                'resnumber' => $transactionId,

            ]);


        })->pay()->render();


        return false;
    }
}
