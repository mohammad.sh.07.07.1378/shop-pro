<?php

namespace App\Http\Controllers\profile;

use App\ActiveCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TokenController extends Controller
{

    public function verifyPhone(Request $request)
    {
        if (!$request->session()->has('phone')) {
            return view('TwoFactorAuth');
        }
        $request->session()->reflash();
        return view('profile.phoneVerify');
    }

    public function verifyPhoneToken(Request $request)
    {
        $this->validateToken($request);

        $status = ActiveCode::verifyCode($request->token, $request->user());

        if ($status) {
            $request->user()->activeCode()->delete();
            $request->user()->update([
                'two_factor_auth' => 'sms',
                'phone' => $request->session()->get('phone')
            ]);
            $request->session()->reflash();
            alert()->success('احراز هویت دو مرحله ایی تایید شد', 'احراز هویت موفق آمیز بود')->persistent('باشه');
        } else {
            alert()->error('احراز هویت دو مرحله ایی تایید نشد', 'احراز هویت نا موفق بود')->persistent('باشه');
        }
        return redirect(route('TwoFactorAuth'));

    }

    /**
     * @param Request $request
     */
    public function validateToken(Request $request): array
    {
        $data = $request->validate([
            'token' => 'required'
        ]);

        return $data;
    }
}
