<?php

namespace App\View\Components;

use Illuminate\View\Component;

class recaptcha extends Component
{
    public $hasError;

    public $clientKey;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(bool $hasError)
    {
        $this->hasError = $hasError;
        $this->clientKey = env('GOOGLE_CAPTCHA_SITE_KEY');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.recaptcha');
    }
}
