<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Discount\Entities\Discount;

class Product extends Model
{
    protected $fillable = [
        'title', 'price', 'description', 'inventory', 'view_count', 'image'
    ];

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class)->using(AttributeProductValue::class)->withPivot(['value_id']);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function gallery()
    {
        return $this->hasMany(ProductGallery::class);
    }
    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }
}
