<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Modules\Discount\Entities\Discount;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'two_factor_auth', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function activeCode()
    {
        return $this->hasMany(ActiveCode::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function isSuperUser()
    {
        return $this->is_superuser;
    }

    public function isStaff()
    {
        return $this->is_staff;
    }

    public function hasTwoFactorAuthOFF()
    {
        return $this->two_factor_auth !== 'off';
    }

    public function hasTwoFactorAuthSMS()
    {
        return $this->two_factor_auth == 'sms';
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRoles($roles)
    {
        //intersect->
        //['x']=[1,2,3,4]
        //['y]=[2,4,6,8]
        // ->true or return because number 2 and 4 in array
        return !!$roles->intersect($this->roles)->all();
    }

    public function hasPermission($permission)
    {
        //contains->
        //['x']=[1,2,3,4]
        //['x]=[2,4,6,8]
        // ->true or return because number 2 and 4 in array

        return $this->permissions->contains('name', $permission->name) || $this->hasRoles($permission->roles);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

}
