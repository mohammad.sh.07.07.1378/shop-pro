<?php

namespace App\Notifications\Channels;

use Ghasedak\Exceptions\ApiException;
use Ghasedak\Exceptions\HttpException;
use Ghasedak\GhasedakApi;
use Illuminate\Notifications\Notification;

class ghasedakChannel
{
    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toGhasedak($notifiable);


        $apiKEY = env('GHASEDAKE_API_KEY');
        try {
            $message = $data['text'];
            $receptor = $data['phone'];
            //this data after register in ghasedak web service https://ghasedak.me/
            $lineNumber = '10008566';
            $api = new GhasedakApi($apiKEY);
            $api->SendSimple($receptor, $message, $lineNumber);
        } catch (ApiException | HttpException $e) {
            throw $e;
        }
    }
}
