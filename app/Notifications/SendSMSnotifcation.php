<?php

namespace App\Notifications;

use App\Notifications\Channels\ghasedakChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendSMSnotifcation extends Notification
{
    use Queueable;

    public $code;

    public $phone;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($code,$phone)
    {
        $this->code=$code;
        $this->phone=$phone;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ghasedakChannel::class];
    }

    public function toGhasedak($notifiable)
    {
        return [
            'text'=>"این شماره اهراز هویت شما در شاپ \n {$this->code}",
            'phone' => $this->phone,
        ];
    }

}
