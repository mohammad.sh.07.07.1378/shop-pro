<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveCode extends Model
{
    protected $fillable = [
        'user_id', 'code', 'expire_at',
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeGenerateCode($query, $user)
    {
        if ($code = $this->getAliveCodeForUser($user)) {
            $code = $code->code;
        } else {
            do {
                $code = mt_rand(100000, 999999);
            } while ($this->checkCodeIsUniqe($user, $code));

            $user->activeCode()->create([
                'code' => $code,
                'expire_at' => now()->addMinute(5)
            ]);

        }
        return $code;
    }

    private function checkCodeIsUniqe($user, int $code)
    {
        return !!$user->activeCode()->whereCode($code)->first();
    }

    private function getAliveCodeForUser($user)
    {
        return $user->activeCode()->where('expire_at', '>', now())->first();

    }

    public function scopeVerifyCode($query, $code, $user)
    {
        return  !! $user->activeCode()->where('code',$code)->where('expire_at', '>', now())->first();
    }


}
