<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::prefix('cart')->namespace('frontend')->group(function () {
    Route::get('/', 'CartController@cart');
    Route::patch('/quantity/change', 'CartController@changeQuantity');
    Route::delete('/delete/{cart}', 'CartController@delete')->name('cart.destroy');
    Route::post('add/cart/{product}', 'CartController@addToCart')->name('add.cart');
});
