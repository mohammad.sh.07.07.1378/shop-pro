<?php

namespace Modules\Cart\Http\Controllers\frontend;


use App\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cart\Helper\Cart;

class CartController extends Controller
{
    public function cart()
    {
        return view('cart::frontend.cart');
    }

    public function addToCart(Product $product)
    {
        if (Cart::has($product)) {
            if (Cart::count($product) < $product->inventory) {
                Cart::update($product, 1);
            } else {
                alert()->error('محصول تموم شده');
            }

        } else {
            Cart::put(
                [
                    'quantity' => 1,
                ],
                $product
            );
        }
        return redirect('/cart');

    }

    public function changeQuantity(Request $request)
    {
        $data = $request->validate([
            'quantity' => 'required',
            'id' => 'required'
        ]);


        if (Cart::has($data['id'])) {

            Cart::update($data['id'], [
                'quantity' => $data['quantity']
            ]);

            return response(['msg' => 'success']);
        }

        return response(['msg' => 'error'], 404);
    }

    public function delete($key)
    {
        Cart::delete($key);

        return back();
    }
}
