<?php

namespace Modules\Cart\Helper;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Static_;

/**
 * Class Cart
 * @package App\Helpers\Cart
 * @method static Cart put(array $value, Model $obj = null)
 * @method Static Boolean has($id)
 * @method Static array get($id)
 * @method Static Collection all()
 */
class Cart extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'cart';
    }
}
