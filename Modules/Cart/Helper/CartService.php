<?php

namespace Modules\Cart\Helper;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Modules\Discount\Entities\Discount;


class CartService
{
    protected $cart;

    public function __construct()
    {
        $this->cart = session()->get('cart') ?? collect([
                'item' => [],
                'discount' => null
            ]);

    }

    /**
     * @param $values
     * @param null $obj
     * @return $this
     */
    public function put(array $values, $obj = null)
    {
        if ($obj instanceof Model) {
            $values = array_merge($values, [
                'id' => Str::random(10),
                'subject_id' => $obj->id,
                'subject_type' => get_class($obj),
                'discount_percent' => 0
            ]);
        } elseif (!isset($values['id'])) {
            $values = array_merge($values, [
                'id' => Str::random(10)
            ]);
        }

        $this->cart['item'] = (collect($this->cart['item'])->put($values['id'], $values))->toArray();

        session()->put('cart', $this->cart);

        return $this;

    }

    public function update($key, $quantity)
    {
        $item = collect($this->get($key, false));

        if (is_numeric($quantity)) {
            $item = $item->merge([
                'quantity' => $item['quantity'] + $quantity
            ]);
        }

        if (is_array($quantity)) {
            $item = $item->merge($quantity);
        }

        $this->put($item->toArray());

        return $this;
    }

    public function count($key)
    {
        if (!$this->has($key)) return 0;

        return $this->get($key)['quantity'];
    }

    public function has($key): bool
    {
        if ($key instanceof Model) {
            return !is_null(
                collect($this->cart['item'])->where('subject_id', $key->id)
                    ->where('subject_type', get_class($key))->first()
            );
        }
        return !is_null(collect($this->cart['item'])->where('id', $key)->first());
    }


    public function get($key, $withRelationShipIfExist = true)
    {
        $item = $key instanceof Model
            ? $this->cart['item']->where('subject_id', $key->id)->where('subject_type', get_class($key))->first()
            : collect($this->cart['item'])->where('id', $key)->first();

        return $withRelationShipIfExist
            ? $this->withRelationShipIfExist($item)
            : $item;
    }

    public function delete($key)
    {
        if ($this->has($key)) {

            $this->cart = collect($this->cart)->filter(function ($item) use ($key) {
                if ($key instanceof Model) {
                    return ($item['subject_id'] != $key['id']) && ($item['subject_type'] != get_class($key));
                }
                return $key != $item['id'];
            });

            session()->put('cart', $this->cart);
            return true;

        }

        return false;
    }


    public function all()
    {
        $cart = $this->cart;
        $cart = collect($this->cart['item'])->map(function ($item) use ($cart) {
            $item = $this->withRelationShipIfExist($item);
            $item = $this->checkDiscountValidate($item, $cart['discount']);
            return $item;
        });

        return $cart;
    }

    private function withRelationShipIfExist($item)
    {
        if (isset($item['subject_id']) && isset($item['subject_type'])) {
            $class = $item['subject_type'];
            $subject = (new $class())->find($item['subject_id']);
            $item[strtolower(class_basename($class))] = $subject;
            unset($item['subject_id']);
            unset($item['subject_type']);
            return $item;
        }
        return $item;
    }

    public function addDiscount($code)
    {
        $this->cart['discount'] = $code;
        session()->get('cart', $this->cart);

    }

    public function getDiscount()
    {
        return Discount::where('code', $this->cart['discount'])->first();
    }

    public function flush()
    {
        $this->cart = collect([
            'item' => [],
            'discount' => null
        ]);
        session()->get('cart', $this->cart);
    }

    protected function checkDiscountValidate($items, $discount)
    {
        $discount = Discount::where('code', $discount)->first();
        if ($discount && $discount->expired_at > now()) {
            if (
                (!$discount->products()->count() && !$discount->categories()->count()) ||
                in_array($items['product']->id, $discount->products()->pluck('id')->toArray()) ||
                array_intersect($items['product']->categories()->pluck('id')->toArray(), $discount->categories()->pluck('id')->toArray())
            ) {
                $items['discount_percent'] = $discount->percent / 100;
            }
        }

        return $items;
    }

}
