@component('admin.layout.content',['title'=>'مقام ها'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل مقام ها</a></li>
        <li class="breadcrumb-item active">لیست مقام ها</li>
    @endslot
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">مقام ها</h3>

                    <div class="card-tools d-flex float-left">
                        <div class="input-group input-group-sm d-flex" style="width: 200px;">
                            <form action="" class="d-flex">
                                <input type="text" name="search" class="form-control float-right"
                                       placeholder="جستجو">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>

                        <div class="float-left  mr-2">
                            @can('create-role')
                                <a class="btn btn-sm btn-info" href="{{route('admin.role.create')}}">ایجاد مقام</a>
                            @endcan
                        </div>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>نام</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($modules as $module)
                            @php
                                $moduleData=new \Nwidart\Modules\Json($module->getPath().'\module.json')
                            @endphp
                            <tr>
                                <td>{{$moduleData->alias}}</td>
                                <td>{{$moduleData->description}}</td>
                                @if(Module::canDisable($module->getName()))
                                    <td class="d-flex btn-sm">
                                        @if(\Module::isEnable($module->getName()))
                                            <form action="{{route('admin.module.disable',['module'=>$module])}}"
                                                  id="{{$module->getName()}}-disable"
                                                  method="post">
                                                @csrf
                                                @method('put')
                                            </form>
                                            <a href="#"
                                               onclick="event.preventDefault();document.getElementById('{{$module->getName()}}-disable').submit()"
                                               class="btn btn-sm btn-danger">غیر فعالسازی</a>
                                        @else
                                            <form action="{{route('admin.module.enable',['module'=>$module])}}"
                                                  id="{{$module->getName()}}-enable"
                                                  method="post">
                                                @csrf
                                                @method('put')
                                            </form>
                                            <a href="#"
                                               onclick="event.preventDefault();document.getElementById('{{$module->getName()}}-enable').submit()"
                                               class="btn btn-sm btn-success ml-2">فعالسازی</a>

                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{--                    {{$modules->render()}}--}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>


@endcomponent
