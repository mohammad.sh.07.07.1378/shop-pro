<?php


Route::get('module', 'MainController@index')->name('module.index');
Route::post('module/{module}/disable', 'MainController@disable')->name('module.disable');
Route::post('module/{module}/enable', 'MainController@enable')->name('module.enable');
