<?php

namespace Modules\Discount\Http\Controllers\frontend;


use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cart\Helper\Cart;
use Modules\Discount\Entities\Discount;

class DiscountController extends Controller
{
    public function cart(Request $request)
    {
        $data = $request->validate([
            'code' => ['required', 'exists:discounts,code']
        ]);

        if (!auth()->check()) {
            return back()->withErrors([
                'code' => 'شما برای اعمال کد تخفیف باید وارد سایت بشین'
            ]);
        }

        $code = Discount::where('code', $data['code'])->first();

        if ($code->expired_at < now()) {
            return back()->withErrors([
                'code' => 'مهلت استفاده گذشته است'
            ]);
        }

        if ($code->users()->count()) {
            if (!in_array(auth()->user()->id, $code->users()->pluck('id')->toArray())) {
                return back()->withErrors([
                    'code' => 'شما قادر به استفاده از این کد نیستید'
                ]);
            }
        }

        Cart::addDiscount($data['code']);

        return back();

    }

    public function delete()
    {
        Cart::addDiscount(null);

        return back();
    }
}
