<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('discount')->group(function () {
    Route::post('cart', 'frontend\DiscountController@cart')->name('cart.discount.code');
    Route::delete('delete', 'frontend\DiscountController@delete')->name('cart.discount.delete');
});
