<?php

use Ghasedak\GhasedakApi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Home Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    \auth()->loginUsingId(1);
    return view('welcome');
})->name('home');

Auth::routes(['verify' => true]);
//Login for google
Route::prefix('auth')->namespace('Auth')->group(function () {
    Route::get('google', 'GoogleAuthController@redirect')->name('auth.google');
    Route::get('callback', 'GoogleAuthController@callback');

});

//verify send code on your phone
Route::prefix('auth')->namespace('Auth')->group(function () {
    Route::get('token', 'AuthTokenController@getToken')->name('auth.token');
    Route::post('token', 'AuthTokenController@postToken');

});

Route::prefix('profile')->namespace('profile')->middleware(['auth'])->group(function () {
    //profile and Two Factor auth
    Route::get('/', 'IndexController@index')->name('profile');
    Route::get('twofactorauth', 'TwoFactorAuthController@twofactorauth')->name('TwoFactorAuth');
    Route::post('twofactorauth', 'TwoFactorAuthController@manegeTwoFactorAuth');
    //phone number verify
    Route::get('twofactorauth/verify', 'TokenController@verifyPhone')->name('verifyPhone');
    Route::post('twofactorauth/verify', 'TokenController@verifyPhoneToken');
    //order list
    Route::get('order', 'OrderController@index')->name('profile.order.list');
    Route::get('order/detail/{order}', 'OrderController@showDetail')->name('profile.order.detail');
    Route::get('order/payment/{order}', 'OrderController@payment')->name('profile.order.payment');

});

Route::get('/home', 'HomeController@index')->name('home');

//product
Route::get('product', 'ProductController@showProduct');
Route::get('product/{product}', 'ProductController@single');


Route::middleware('auth')->group(function () {
    Route::post('comment', 'HomeController@comment')->name('send.comment');
    Route::post('cart/payment', 'PaymentController@payment')->name('cart.payment');
    Route::get('payment/callback', 'PaymentController@callback')->name('payment.callback');
});

