<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.index');
});

Route::resource('users', 'user\UsersController');

Route::get('/{user}/permission', 'user\PermissionController@create')->name('user.permission')->middleware('can:staff-user-permission');
Route::post('/{user}/permission', 'user\PermissionController@store')->name('user.permission.store')->middleware('can:staff-user-permission');

Route::resource('permission', 'PermissionController');

Route::resource('role', 'RoleController');

Route::resource('product', 'ProductController');

Route::get('comments/unapproved', 'CommentsController@unapproved')->name('comments.unapproved');
Route::resource('comments', 'CommentsController');

Route::resource('category', 'CategoryController');

Route::post('/attribute/values', 'AttributeController@getValues');

Route::resource('orders', 'OrderController');
Route::get('order/{order}/payment', 'OrderController@payment')->name('order.payment');

Route::resource('product.gallery', 'ProductGalleryController');
